# -*- coding: utf-8 -*-
import scrapy


class RedditPaginationSpider(scrapy.Spider):
    name = "reddit-pagination"
    allowed_domains = ["reddit.com"]
    start_urls = ['http://www.reddit.com/r/programming']
    custom_settings = {
        'DEPTH_LIMIT': 4,
    }

    def parse(self, response):
        for submission_sel in response.css("div.entry"):
            yield {
                'category': response.css("span.redditname > a ::text").extract_first(),
                'title': submission_sel.css("a.title ::text").extract_first(),
                'user': submission_sel.css("a.author ::text").extract_first()
            }
        next_page = response.xpath(
            "//span[@class='nextprev']/a[contains(@rel, 'next')]/@href"
        ).extract_first()
        if next_page:
            yield scrapy.Request(next_page, callback=self.parse)
