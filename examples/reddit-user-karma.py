import scrapy


class RedditKarmaSpider(scrapy.Spider):
    name = 'reddit-karma'
    allowed_domains = ['reddit.com']
    start_urls = ['http://reddit.com/r/programming']

    def parse(self, response):
        for submission in response.css("div.entry"):
            user_url = submission.css("a.author ::attr(href)").extract_first()
            yield scrapy.Request(user_url, callback=self.parse_user_page)

    def parse_user_page(self, response):
        yield {
            'username': response.css('div.titlebox h1::text').extract_first(),
            'karma': response.css('span.karma ::text').extract_first(),
            'url': response.url
        }

