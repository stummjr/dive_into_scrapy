import scrapy


class RedditSpider(scrapy.Spider):
    name = 'reddit'
    subreddits = ['programming', 'python', 'learnpython', 'datascience']

    def start_requests(self):
        for subreddit in self.subreddits:
            yield scrapy.Request('http://reddit.com/r/%s' % subreddit)

    def parse(self, response):
        for submission in response.css("div.entry"):
            yield {
                'url': submission.css("a.title ::attr(href)").extract_first(),
                'title': submission.css("a.title ::text").extract_first(),
                'user': submission.css("a.author ::text").extract_first(),
                'comments': submission.css("a.comments ::text").extract_first(),
                'domain': submission.css("span.domain a ::attr(href)").extract_first()
            }
