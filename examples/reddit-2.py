import scrapy


class RedditSpider(scrapy.Spider):
    name = "reddit"
    start_urls = [
        'http://www.reddit.com/r/programming',
        'http://www.reddit.com/r/python',
        'http://www.reddit.com/r/learnpython',
        'http://www.reddit.com/r/datascience'
    ]

    def parse(self, response):
        for submission in response.css("div.entry"):
            yield {
                'url': submission.css("a.title ::attr(href)").extract_first(),
                'title': submission.css("a.title ::text").extract_first(),
                'user': submission.css("a.author ::text").extract_first(),
                'comments': submission.css("a.comments ::text").extract_first(),
                'domain': submission.css("span.domain a ::attr(href)").extract_first()
            }
