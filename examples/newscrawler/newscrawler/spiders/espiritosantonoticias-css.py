# -*- coding: utf-8 -*-
import scrapy


class EspiritosantonoticiasSpider(scrapy.Spider):
    name = "espiritosantonoticias-css"
    allowed_domains = ["espiritosantonoticias.com.br"]
    start_urls = (
        'http://www.espiritosantonoticias.com.br/',
    )
    download_delay = 1.0

    def parse(self, response):
        for url in response.css('div#menus a ::attr(href)').extract():
            if 'category' in url:
                yield scrapy.Request(url, callback=self.parse_category_page)

    def parse_category_page(self, response):
        article_links = response.css('div#artigohome > a ::attr(href)').extract()
        for alink in article_links:
            yield scrapy.Request(alink, callback=self.parse_article)
        next_page = response.css("div.recentes > a::attr(href)").extract_first()
        if next_page:
            yield scrapy.Request(next_page, callback=self.parse_category_page)

    def parse_article(self, response):
        yield {
            'title': response.css("h1.pagetitle::text").extract_first(),
            'time': response.css("time::attr(datetime)").extract_first(),
            'author': response.css("div#mdautor b ::text").extract_first()
        }
