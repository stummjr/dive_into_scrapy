# -*- coding: utf-8 -*-

BOT_NAME = 'newscrawler'

SPIDER_MODULES = ['newscrawler.spiders']
NEWSPIDER_MODULE = 'newscrawler.spiders'


# Enable and configure HTTP caching (disabled by default)
HTTPCACHE_ENABLED=True
