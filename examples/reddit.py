import scrapy


class RedditSpider(scrapy.Spider):
    name = "reddit"

    start_urls = [
        'http://www.reddit.com/r/programming',
    ]

    def parse(self, response):
        for submission in response.css("div.entry"):
            yield {
                'url': submission.css("a.title ::attr(href)").extract_first(),
                'title': submission.css("a.title ::text").extract_first(),
                'user': submission.css("a.author ::text").extract_first()
            }

