import scrapy


class SpidyQuotesSpider(scrapy.Spider):
    name = 'spidyquotes'
    start_urls = [
        'http://spidyquotes.herokuapp.com/'
    ]

    def parse(self, response):
        for quote in response.css('div.quote'):
            yield {
                'text': quote.css('span::text').extract_first(),
                'author': quote.css('small::text').extract_first(),
                'tags': quote.css('.tags a::text').extract(),
            }
